// TASK 1
const person = {
    name: 'John',
    age: 30,
    gender: 'male'
};

let newPerson = {...person}
newPerson.age = 35

console.log(person); // { name: 'John', age: 30, gender: 'male' }
console.log(newPerson); // { name: 'John', age: 35, gender: 'male' }


// TASK 2
const car = {
    make: 'Toyota',
    model: 'Corolla',
    year: 2015
};

function printCarInfo(car) {
    if (car.year >= 2001) {
        console.log(`Make: ${car.make}, Model: ${car.model}, Year: ${car.year}"`)
    } else console.log('Машина занадто стара.')
}

printCarInfo(car);

// TASK 3
// Створіть рядок тексту str із будь-яким вмістом. Далі створіть функцію countWords, яка приймає рядок тексту як аргумент та повертає кількість слів у рядку.

const str = 'JavaScript is a high-level programming language';

function countWords(str) {
    let arr = str.split(' ')
    return arr.length;
}

console.log(countWords(str));


// TASK 4
// Створіть рядок тексту str із будь-яким вмістом. Далі створіть функцію reverseString, яка приймає рядок тексту як аргумент та повертає його в оберненому порядку.

const str = 'JavaScript is awesome!';

function reverseString(str) {
    let splitString = str.split("").reverse().join("");
    return splitString;
}

console.log(reverseString(str)); // '!emosewa si tpircSavaJ'

// TASK 5
// Створіть рядок тексту str із будь-яким вмістом. Далі створіть функцію reverseWordsInString, яка приймає рядок тексту як аргумент та повертає його з розвернутими словами.

const str = 'JavaScript is awesome!';

function reverseWordsInString(str) {
    let splitString = str.split(" ")
    for (let i = 0; i < splitString.length; i++) {
        splitString[i] = splitString[i].split("").reverse().join("");
    }
    let str1 = splitString.join(" ");
    return str1;
}

console.log(reverseWordsInString(str)); // 'tpircSavaJ si !emosewa'
