// TASK 1

let r = Math.random()
let min = Math.trunc(r * 59) + 1;

if (min < 15) {
    console.log(min, 'перша чверть')
}
if (min < 30 && min > 15) {
    console.log(min, 'друга чверть')
}
if (min < 45 && min > 30) {
    console.log(min, 'третя чверть')
}
if (min < 60 && min > 45) {
    console.log(min, 'четверта чверть')
}

// TASK 2
let lang = 'ua';
let arr


if (lang === 'ua') {
    console.log(arr = 'понеділок,', 'вівторок,', 'середа,', 'четвер,', "п'ятниця,", 'субота,', 'неділя')
} else {
    console.log(arr = 'monday,', 'tuesday,', 'wednesday,', 'thursday,', "friday,", 'saturday,', 'sunday')
}

switch (lang) {
    case('ua'):
        console.log(arr = 'понеділок,', 'вівторок,', 'середа,', 'четвер,', "п'ятниця,", 'субота,', 'неділя');
        break;
    case('en'):
        console.log(arr = 'monday,', 'tuesday,', 'wednesday,', 'thursday,', "friday,", 'saturday,', 'sunday');
        break;
    default:
        console.log(`введи правильне значення замість ${lang}.`);
}

// АБО

let lan = 'ua';
let ar = [
    ["понеділок", "вівторок", "середа", "четвер", "пятниця", "субота", "неділя"],
    ["monday", "tuesday", "wednesday", "thursday", "friday", "saturday", "sunday"]
];


if (lan === 'ua') {
    console.log(ar[0])
} else {
    console.log(ar[1])
}


// TASK 3
let random = Math.floor(Math.random() * 900000) + 100000;
let str=random.toString();
console.log(str);

let a = +str[0] + +str[1] + +str[2];
let b = parseInt(str[3]) + parseInt(str[4]) + parseInt(str[5]);

console.log(a, b);

a === b ? console.log('Щасливий!'):console.log('Вам не пощастило')
